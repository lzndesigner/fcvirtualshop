<?php
class ModelExtensionTotalAVista extends Model {
	public function getTotal($total) {

		if ($this->config->get('total_avista_status')) {

			$total_pedido = $this->config->get('total_avista_total_pedido');

			$methods_aplicaveis = explode(",", $this->config->get('total_avista_methods'));
			
			if (isset($this->session->data['payment_method']['code'])) $paymethod = $this->session->data['payment_method']['code'];
			
			if (isset($paymethod)) {
				if (in_array($paymethod, $methods_aplicaveis)) {
					$this->load->language('extension/total/avista');
					if($total > $total_pedido){
						$float = floatval(($this->config->get('total_avista_total')<10) ? '0.0'.str_replace(array(',','.'),'',$this->config->get('total_avista_total')) : '0.'.str_replace(array(',','.'),'',$this->config->get('total_avista_total')));
						// var_dump($total['total']);
						$percent = $total['total'] * $float;				

						$total['totals'][] = array( 
							'code'		 => 'total_avista',
							'title'      => $this->language->get('text_discount') . $this->config->get('total_avista_total'). '%',
							'value'      => $percent*-1,
							'sort_order' => $this->config->get('total_avista_sort_order')
						);
						$total['total'] -= $percent;
					}else if($paymethod == 'bank_transfer'){
								
						$total['totals'][] = array( 
							'code'		 => 'total_avista',
							'title'      => 'Acima de R$ '.$total_pedido .'<br/> Desconto de ' . $this->config->get('total_avista_total'). '%'.' para Depósito Bancário',
							'value'      => '',
							'sort_order' => $this->config->get('total_avista_sort_order')
						);
					}
				}
			}
			
		}
	}
}
?>