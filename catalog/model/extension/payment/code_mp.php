<?php

/**
 * Class ModelExtensionPaymentCodeMP
 *
 * © Copyright 2014-2019 Codemarket - Todos os direitos reservados.
 *
 * @author Codemarket - Felipo Antonoff Araújo e Victor Henrique Ramos
 * @version 1.0
 * @package code_mp
 */
class ModelExtensionPaymentCodeMP extends Model
{
    private $log;

    /**
     * Recebe a configuracao do painel
     *
     * @return \stdClass
     */
    private function getConfig($address, $total)
    {
        $this->load->model('module/codemarket_module');
        $config = $this->model_module_codemarket_module->getModulo('562');
        $status = true;

        $this->log = new Log('Code-MercadoPago-Cartao.log');

        if (empty($config) || empty($config->code_production_key) || empty($config->code_production_token)
            || empty($config->code_sandbox_key) || empty($config->code_sandbox_token)
            || empty($config->code_title_cartao) || empty($config->code_env_cartao) || !isset($config->code_sort_order_cartao)
            || empty($config->code_status_cartao) || $config->code_status_cartao == 2
        ) {
            $this->log->write('Model getConfig() - Módulo desativado ou configurado errado. Verificar a Configuração do módulo');
            $status = false;
        }

        //Região
        if (!empty($config->code_geo_zones_cartao)) {
            $query = $this->db->query("
                SELECT geo_zone_id FROM " . DB_PREFIX . "zone_to_geo_zone WHERE 
                geo_zone_id = '" . (int) $config->code_geo_zones_cartao . "' 
                AND country_id = '" . (int) $address['country_id'] . "' 
                AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')
                LIMIT 1
            ");

            if (empty($query->row['geo_zone_id'])) {
                $this->log->write('Model getConfig() - Região Geográfica não encontrada' . print_r($query->row, true));

                $status = false;
            }
        }

        /*
            Modo Cliente de Teste
            Desativa se não for o cliente de teste e estiver habilitado o modo de teste
        */
        if(!empty($config->code_test) && $config->code_test == 1 && !empty($config->code_test_email)){
            if (!empty($this->customer->getEmail()) && $config->code_test_email != $this->customer->getEmail()) {
                $status = false;
            } else if (empty($this->customer->getEmail())) {
                $status = false;
            }
        }

        if ($config->code_env_cartao === 'production') {
            $config->code_env_mensagem = '';
        } else {
            $config->code_env_mensagem = " - <b>Modo de Teste, não compre!</b>";
        }

        $config->code = 'code_mp';
        $config->title = $config->code_title_cartao.$config->code_env_mensagem;
        $config->terms = '';
        $config->sort_order = $config->code_sort_order_cartao;
        $config->status = $status;

        return $config;
    }

    /**
     * Metodo padrao pra exibicao na listagem
     *
     * @param $address
     * @param $total
     *
     * @return array
     */
    public function getMethod($address, $total)
    {
        $config = $this->getConfig($address, $total);

        if ($config->status) {
            return [
                'code'       => $config->code,
                'title'      => $config->title,
                'terms'      => $config->terms,
                'sort_order' => $config->sort_order,
            ];
        }

        return [];
    }
}
