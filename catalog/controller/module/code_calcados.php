<?php

//Aumentando para executar por mais tempo

set_time_limit(0);
ini_set('mysql.connect_timeout', '0');
ini_set('max_execution_time', '0');
ini_set('memory_limit', '256M');

class ControllerModuleCodeCalcados extends Controller
{
    private $conf;
    private $log;
    private $languages;

    /**
     *
     * ControllerModuleCode454 constructor.
     * © Copyright 2014-2021 Codemarket - Todos os direitos reservados.
     *
     * @param $registry
     *
     * @throws \Exception
     */
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model('module/codemarket_module');
        $conf = $this->model_module_codemarket_module->getModulo('605');
        $this->log = new log('Code-Revenda-Calcados.log');

        if (
            empty($conf) || empty($conf->code_endereco) || empty($conf->code_quantidade_minima_estoque) ||
            empty($conf->code_token) || $conf->code_token != $this->request->get['token'] ||
            empty($conf->code_habilitar) || $conf->code_habilitar != 1 || !isset($conf->code_preco) ||
            empty($conf->code_preco_tipo) || empty($conf->code_preco_venda) || empty($conf->code_status_sem_estoque) ||
            empty($conf->code_opcao_id) || empty($conf->code_ativar_produto)
        ) {
            $this->log->write('construct() - Módulo desativado, verifique a configuração');
            exit("Token inválido ou desabilitado");
        }

        $this->conf = $conf;

        $this->load->model('localisation/language');
        $this->languages = $this->model_localisation_language->getLanguages();

        $this->log->write('construct() - Passou na verificação');
        return true;
    }

    /**
     *
     */
    public function clear()
    {
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_description`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_image`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_option`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_option_value`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_to_category`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_to_layout`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "product_to_store`");
        //$this->db->query("TRUNCATE `" . DB_PREFIX . "option`");
        //$this->db->query("TRUNCATE `" . DB_PREFIX . "option_description`");
        //$this->db->query("TRUNCATE `" . DB_PREFIX . "option_value`");
        //$this->db->query("TRUNCATE `" . DB_PREFIX . "option_value_description`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "category`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "category_filter`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "category_path`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "category_to_store`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "manufacturer`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "manufacturer_to_store`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "code_revendadecalcados_category`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "code_revendadecalcados_manufacturer`");
        $this->db->query("TRUNCATE `" . DB_PREFIX . "code_revendadecalcados_product`");

        echo "Truncate nas tabelas dos Produtos, Categorias, Opções, Fabricante e da Revenda";
    }

    /**
     * Gera o cache do XML da Revenda Calçados
     */
    public function cache()
    {
        $this->log->write('cache() - Rodando o Cache');

        $url = $this->conf->code_endereco;
        $folder = DIR_CACHE . 'code/calcados/';
        $xml = file_get_contents($url);

        if (strpos('Excedeu limite de consulta', $xml) === false) {
            @mkdir($folder, 0755, true);
            @file_put_contents($folder . 'data.xml', $xml);

            @unlink('info.json');
            @file_put_contents($folder . 'info.json', json_encode([
                'date' => date('d/m/Y H:i:s'),
            ]));
        }

        $this->log->write('cache() - Rodado com sucesso');
    }

    /**
     *
     * Retorna o Produto da tabela code_revendadecalcados_product
     *
     * @param string $ref
     *
     * @return array|null
     */
    private function getProduct(string $ref)
    {
        $product = $this->db->query("
            SELECT * FROM `" . DB_PREFIX . "product` p
            INNER JOIN " . DB_PREFIX . "code_revendadecalcados_product rc ON (rc.product_id = p.product_id)
            WHERE rc.revenda_referencia = '" . $this->db->escape($ref) . "' LIMIT 1
        ")->row;

        if ($product) {
            return (array) $product;
        }

        return null;
    }

    public function addProducts()
    {
        echo "Adicionando Produtos<br>";
        $this->log->write('addProducts() - Adicionando Produtos');

        $time_total = microtime(true);

        $this->cache();

        if (is_file(DIR_CACHE . 'code/calcados/data.xml')) {
            $xml = simplexml_load_file(DIR_CACHE . 'code/calcados/data.xml', 'SimpleXMLElement', LIBXML_NOCDATA);
            $xml = json_decode(json_encode((array) $xml), true);
        } else {
            $this->log->write('addProducts() - Sem o XML');
            exit("Sem o XML");
        }

        if (empty($xml)) {
            $this->log->write('addProducts() - XML vazio');
            exit("Sem dados no XML");
        }

        if (!empty($xml['mensagem']) && $xml['mensagem'] != 'OK') {
            $this->log->write('addProducts() - Problema para puxaro XML, mensagem: ' . $xml['mensagem']);
            exit("Problema para baixar o XML, mensagem: " . $xml['mensagem']);
        }

        //print_r($xml); exit();
        $c = 0;
        $updates = 0;

        $q = !empty($this->request->get['quantity']) ? (int) $this->request->get['quantity'] : 10000;

        foreach (array_slice($xml['produto'], 0, $q) as $key => $p) {
            $productReference = $this->getProduct($p['referencia']);

            if (!empty($productReference)) {
                $t = microtime(true);
                $this->log->write('addProducts() - Produto: ' . $productReference['product_id'] . ' já cadastrado');

                $this->editProductDatabase($productReference['product_id'], $p);
                $updates++;

                echo '-------------------<br>';
                echo "Tempo total para atualizar o Produto: " . $productReference['product_id'] . " " . $p['nome'] . " " . number_format((microtime(true) - $t), 2) . "s<br>";
                echo '-------------------<br>';

                continue;
            }

            $productDate = $this->mountProduct($p);
            if (!empty($productDate)) {
                $t = microtime(true);
                $product_id = $this->addProductDatabase($productDate);

                echo '-------------------<br>';
                echo "Tempo total para adicionar o Produto: " . $product_id . " " . $p['nome'] . " " . number_format((microtime(true) - $t), 2) . "s<br>";
                echo '-------------------<br>';
                $c++;
            }
        }

        echo "Produtos adicionados com sucesso";

        echo '-------------------<br>';
        echo "Tempo total para importador " . $c . " e atualizar " . $updates . " Produtos  " . number_format((microtime(true) - $time_total), 2) . "s<br>";
        echo '-------------------<br>';

        $this->log->write('addProducts() - Adicionado Produtos');
    }

    private function mountPriceQuantity($p)
    {
        //Preço de Venda pode ser Atacado ou Dropshipping
        if ($this->conf->code_preco_venda == '1' && !empty($p['valor_dropshipping'])) {
            $price = $p['valor_dropshipping'];
        } else if (!empty($p['valor_atacado'])) {
            $price = $p['valor_atacado'];
        } else {
            $this->log->write('mountPriceQuantity() - Produto sem preço');
            return false;
        }

        $this->log->write('mountPriceQuantity() - Preço: ' . $price);

        if ($this->conf->code_preco_tipo == 'p' && !empty($this->conf->code_preco)) {
            $porc = 1 + (float) ((float) $this->conf->code_preco / 100);
            $this->log->write('mountPriceQuantity() - Porcentagem: ' . $porc);
            $price = $price * $porc;
            $price = number_format($price, 2, '.', '');
            $this->log->write('mountPriceQuantity() - Preço Final porcentagem: ' . $price);
        } else if (!empty($this->conf->code_preco)) {
            $price = $price + (float) $this->conf->code_preco;
            $price = number_format($price, 2, '.', '');
            $this->log->write('mountPriceQuantity() - Preço Final fixo: ' . $price);
        }

        //Soma os Estoques de todas as Opções
        $stockTotal = 0;
        $options = [];

        //Quando só tem uma Opção, não tem a posição 0 no array
        if (isset($p['estoque']['quantidade'])) {
            $stock = $p['estoque'];

            $stockTotal += !empty($stock['quantidade']) ? (int) $stock['quantidade'] : 0;

            if (!empty($stock['tamanho'])) {
                $options[] = [
                    'name'     => 'Tamanho',
                    'value'    => $stock['tamanho'],
                    'quantity' => (int) !empty($stock['quantidade']) ? $stock['quantidade'] : 0,
                ];
            }
        } else {
            foreach ($p['estoque'] as $stock) {
                $stockTotal += !empty($stock['quantidade']) ? (int) $stock['quantidade'] : 0;

                if (!empty($stock['tamanho'])) {
                    $options[] = [
                        'name'     => 'Tamanho',
                        'value'    => $stock['tamanho'],
                        'quantity' => (int) !empty($stock['quantidade']) ? $stock['quantidade'] : 0,
                    ];
                }
            }
        }

        return [
            'price'      => $price,
            'stockTotal' => $stockTotal,
            'options'    => $options,
        ];
    }

    /**
     * Monta os dados do Produto
     *
     * @param $p
     *
     * @return array|false
     */
    private function mountProduct($p)
    {
        //Se vazio o Peso e Dimensões, não adiciona o Produto
        if (empty($p['descricao']) || empty($p['peso_gramas']) || empty($p['dimensao_caixa_cm'])) {
            $this->log->write('mountProduct() - Produto sem os dados mínimos');
            return false;
        }

        $data = $this->mountPriceQuantity($p);
        if (empty($data['price'])) {
            return false;
        }

        $price = $data['price'];
        $stockTotal = $data['stockTotal'];
        $options = $data['options'];

        if ($this->conf->code_quantidade_minima_estoque > $stockTotal
        ) {
            $this->log->write('mountProduct() - Produto com menos estoque, que o configurado, estoque: ' . $stockTotal);
            return false;
        }

        //Dimensões
        if (!empty($p['dimensao_caixa_cm'])) {
            $dims = explode(' x ', $p['dimensao_caixa_cm']);
        } else {
            $dims = [0, 0, 0];
        }

        //Retorna o array das Categorias criadas
        $categorias = $this->addCategory($p);

        if (!empty($p['marca']) && strtolower($p['marca']) != 'sem marca') {
            $fabricante = trim($p['marca']);
            $fabricante_id = $this->fabricante($fabricante);
        } else {
            $fabricante = '';
            $fabricante_id = '';
        }

        //Informe o ID do Status para quando não tem Estoque
        $estoque_status = (int) $this->conf->code_status_sem_estoque;
        //Digite 1 se quiser que de baixa no Estoque ao vender ou 0 para não
        $subtrair = 1;
        //Informe a quantidade mínima no estoque para vender o Produto, caso não tenha mínimo, digite 0
        $minimo = 1;
        //Informe 1 se tem entrega ou 0 se não precisa de entrega no Produto
        $entrega = 1;

        //Descrição
        foreach ($this->languages as $l) {
            /*
            [name] => Nome
            [description] => <p>Descricao</p>
            [meta_title] => Nome
            [meta_description] => Nome
            [meta_keyword] => Nome
            [tag] =>
             */

            if (!empty($this->conf->code_titulo_referencia) && $this->conf->code_titulo_referencia == 1) {
                $name = trim($p['nome']) . ' - ' . trim($p['referencia']);
            } else {
                $name = trim($p['nome']);
            }

            $lin[$l['language_id']] = [
                'name'             => $name,
                'description'      => trim($p['descricao']),
                'meta_title'       => trim($p['nome']),
                'meta_description' => trim($p['descricao']),
                'meta_keyword'     => trim($p['nome']),
                'tag'              => '',
            ];
        }

        return [
            'product_description' => $lin,
            'model'               => 'RC' . $p['referencia'],
            'price'               => (float) $price,
            'quantity'            => $stockTotal,
            'minimum'             => $minimo,
            'upc'                 => $p['referencia'],
            'tax_class_id'        => 0,
            'subtract'            => $subtrair,
            'stock_status_id'     => $estoque_status,
            'shipping'            => $entrega,
            'keyword'             => $this->slug($name),
            'length'              => (float) $dims[0],
            'width'               => (float) $dims[1],
            'height'              => (float) $dims[2],
            'length_class_id'     => 1,
            'weight'              => (float) $p['peso_gramas'],
            'weight_class_id'     => 2,
            'sort_order'          => 1,
            'product_store'       => [0],
            'product_category'    => $categorias,
            'manufacturer'        => $fabricante,
            'manufacturer_id'     => $fabricante_id,
            'options'             => $options,
            'rc'                  => $p,
        ];
    }

    /**
     * Adiciona imagem aos Produtos
     *
     * @return bool
     */
    public function addImagesProducts()
    {
        echo "Adicionando Imagens nos Produtos<br>";
        $time_total = microtime(true);

        /*
        if (empty($this->conf->code_imagem_importar) || $this->conf->code_imagem_importar == 2) {
            $this->log->write('addImagesProducts - Configurado para não importar imagens');
            return true;
        }
        */

        $this->log->write('addImagesProducts - Adicionando imagens');

        $q = !empty($this->request->get['quantity']) ? (int) $this->request->get['quantity'] : 10000;
        $time = !empty($this->request->get['time']) ? (int) $this->request->get['time'] : 80;

        $query = $this->db->query("SELECT p.product_id, p.*, rc.data, rc.slug, rc.code_revendadecalcados_product_id FROM `" . DB_PREFIX . "product` p
            INNER JOIN " . DB_PREFIX . "code_revendadecalcados_product rc ON (rc.product_id = p.product_id)
            WHERE rc.image_check = 0 LIMIT " . (int) $q . "
        ");

        if (empty($query->rows)) {
            $this->log->write('addImagesProducts - Imagens já verificadas');
            echo "Imagens já adicionadas e verificadas<br>";
            return true;
        }

        $produtos = $query->rows;

        //Listando os Produtos
        foreach ($produtos as $p) {
            $t = microtime(true);
            $this->log->write('addImagesProducts - Adicionando imagem para o Produto: ' . $p['product_id']);
            $this->saveImage($p);

            echo '-------------------<br>';
            echo "Tempo para adicionar as imagens do Produto: " . $p['product_id'] . " " . number_format((microtime(true) - $t), 2) . "s<br>";
            echo '-------------------<br>';

            if (number_format((microtime(true) - $time_total), 2) > $time) {
                $this->log->write('addImagesProducts - Tempo Limite - Rodou por: ' . number_format((microtime(true) - $time_total)) . 's');
                exit("Tempo Limite - Rodou por: " . number_format((microtime(true) - $time_total)) . "s");
            }
        }

        $this->log->write('addImagesProducts - Adicionado imagens com sucesso');

        echo '-------------------<br>';
        echo "Rodado com sucesso para " . count($produtos) . " Produtos, tempo total para adicionar: " . number_format((microtime(true) - $time_total), 2) . "s<br>";
        echo '-------------------<br>';

        return true;
    }

    /**
     * Salva a url da imagem no Banco e a imagem nos arquivos
     *
     * @param $product
     *
     * @return bool
     */
    private function saveImage($product)
    {
        $folder = 'catalog/rcalcados/';
        @mkdir(DIR_IMAGE . $folder, 0755);
        $path = DIR_IMAGE . $folder;

        $revenda = json_decode($product['data'], true);
        $image_types_allowed = [
            'image/gif'  => '.gif',
            'image/jpeg' => '.jpg',
            'image/png'  => '.png',
            'image/webp' => '.webp',
        ];

        $extension = '';

        if (empty($revenda['fotos'][0])) {
            $this->log->write('saveImage - Sem imagens para adicionar do produto: ' . $product['product_id']);
            return true;
        }

        foreach ($revenda['fotos'] as $key => $img) {
            if (empty($img['url_foto'])) {
                continue;
            }

            $image = $img['url_foto'];

            //Verificando a extensão e Path
            $image_info = getimagesize($image);

            foreach ($image_types_allowed as $type => $ext) {
                if ($type == $image_info['mime']) {
                    $extension = $ext;
                }
            }

            if (empty($extension)) {
                return false;
            }

            $slug = $this->slug($revenda['nome']) . '-' . $product['product_id'] . '-' . ($key + 1);
            $filePath = $folder . $slug . $extension;

            //Verificando se já foi adicionada
            if ((is_file($path . $slug . $extension) and filesize($path . $slug . $extension) > 2000)) {
                $this->db->query("
                    UPDATE " . DB_PREFIX . "code_revendadecalcados_product SET
                    image_check = 1
                    WHERE product_id = '" . (int) $product['product_id'] . "'"
                );

                $this->log->write('saveImage - Imagens já adicionada, marcando no Banco');
                return true;
            }

            $imgDownload = $this->getImage($image);

            if (empty($imgDownload['image'])) {
                $this->log->write('saveImage - não foi possível baixar a imagem');
                return false;
            }

            file_put_contents($path . $slug . $extension, $imgDownload['image']);
            $this->log->write('saveImage - Imagem  ' . $slug . ' adicionada no Produto: ' . $product['product_id']);

            //Capa
            if ($key == 0) {
                //Primeira imagem, usar como capa
                $this->db->query("
                    UPDATE " . DB_PREFIX . "product SET
                    image = '" . $this->db->escape($filePath) . "'
                    WHERE product_id = '" . (int) $product['product_id'] . "'"
                );
            } else {
                //Se quiser na miniatura a capa, precisa tirar do else o Insert abaixo, mas vai ficar 3 imagens se tem 1 principal e 1 extra
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "product_image SET 
                    product_id = '" . (int) $product['product_id'] . "', 
                    image = '" . $this->db->escape($filePath) . "', 
                    sort_order = '" . (int) ($key + 1) . "'
                ");
            }
        }

        $this->log->write('saveImage - Imagens salva com sucesso');
        return true;
    }

    /**
     * @param $url string
     *
     * @return bool|mixed
     */
    private function getImage($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_TIMEOUT        => 180,
            CURLOPT_CONNECTTIMEOUT => 35,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER     => [
                "Cache-Control: no-cache",
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->log->write('Error Curl' . print_r($err, true));
        } else {
            return ['image' => $response];
        }
    }

    //-------- MÉTODOS OPENCART - DATABASE --------

    /**
     * Adiciona os Produtos no Banco de Dados
     *
     * @param array $data
     *
     * @return int product_id
     */
    private function addProductDatabase($data)
    {
        $this->log->write('addProductDatabase() - Dentro ' . print_r($data, true));

        $status = $this->conf->code_ativar_produto == 1 ? 1 : 0;

        $this->db->query("
        INSERT INTO " . DB_PREFIX . "product SET
            model = '" . $this->db->escape($data['model']) . "',
            upc = '" . $this->db->escape($data['upc']) . "',
            sku = '" . $this->db->escape($data['upc']) . "',
            ean = '" . $this->db->escape($data['upc']) . "',
            quantity = '" . (int) $data['quantity'] . "',
            minimum = '" . (int) $data['minimum'] . "',
            subtract = '" . (int) $data['subtract'] . "',
            stock_status_id = '" . (int) $data['stock_status_id'] . "',
            date_available = NOW(),
            shipping = '" . (int) $data['shipping'] . "',
            price = '" . $data['price'] . "',
            weight = '" . $data['weight'] . "',
            weight_class_id = '" . (int) $data['weight_class_id'] . "',
            length = '" . $data['length'] . "',
            width = '" . $data['width'] . "',
            height = '" . $data['height'] . "',
            length_class_id = '" . (int) $data['length_class_id'] . "',
            status = $status,
            tax_class_id = '" . (int) $data['tax_class_id'] . "',
            sort_order = '" . (int) $data['sort_order'] . "',
            date_added = NOW()
        ");

        $product_id = $this->db->getLastId();

        //Adicionado na tabela da Revenda Calçados
        $this->db->query("INSERT INTO " . DB_PREFIX . "code_revendadecalcados_product SET
            product_id = '" . (int) $product_id . "',
            revenda_referencia = '" . $data['rc']['referencia'] . "',
            revenda_price_atacado = '" . $data['rc']['valor_atacado'] . "',
            revenda_price_dropshipping = '" . $data['rc']['valor_dropshipping'] . "',
            revenda_quantity = '" . (int) $data['quantity'] . "',
            revenda_status = '" . (int) $data['rc']['reposicao_estoque'] . "',
            slug = '" . $this->db->escape($data['keyword']) . "',
            image_check = '" . (int) 0 . "',
            data = '" . $this->db->escape(json_encode($data['rc'])) . "',
            date_added = NOW(),
            date_modified = NOW()
        ");

        $this->log->write('addProductDatabase() - Salvo o Produto: ' . $product_id);

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");

            if (!empty($data['keyword'])) {
                $query = $this->db->query("SELECT keyword  FROM `" . DB_PREFIX . "seo_url` WHERE  keyword  = '" . $this->db->escape($data['keyword']) . "' AND language_id = '" . (int) $language_id . "' LIMIT 1");

                //URL Amigável
                if (!isset($query->row['keyword'])) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = 0, language_id = '" . (int) $language_id . "', query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
                }
            }
        }

        if (!empty($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        if (!empty($data['product_image'])) {
            foreach ($data['product_image'] as $i => $product_image) {
                if ($i == 1) {
                    continue;
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape($product_image) . "', sort_order = '" . (int) $i . "'");
            }
        }

        //Adicionando opções
        if (!empty($data['options'])) {
            $this->addOptionDatabase($product_id, $data['options']);
        }

        if (!empty($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT IGNORE INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        if (!empty($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }

        $customer_groups = $this->getCustomerGroups();

        //Criando promoção
        /*
        if (!empty($data['rc']['valor_promocao']) && !empty($data['rc']['data_inicio_promocao']) && !empty($data['rc']['data_fim_promocao'])) {
            foreach ($customer_groups as $cg) {
                $this->addSpecialDatbase($product_id, $cg['customer_group_id'], 1, $data['rc']['valor_promocao'], $data['rc']['data_inicio_promocao'], $data['rc']['data_fim_promocao']);
            }
        }
        */

        return $product_id;
    }

    private function addOptionDatabase($product_id, $options)
    {
        $this->log->write('addOptionDatabase -  Adicionando Opções Produto: ' . $product_id);

        $required = 1;
        $option_id = $this->conf->code_opcao_id;

        $subtract = 1;
        $price = 0;
        $price_prefix = '+';
        $points = 0;
        $points_prefix = '+';
        $weight = 0;
        $weight_prefix = '+';

        //Só um tipo Tamanho
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '" . (int) $option_id . "', required = '" . (int) $required . "'");
        $product_option_id = $this->db->getLastId();

        $this->log->write('addOptionDatabase -  Criado o product_option ' . $product_option_id);

        $sort_order = 1;
        foreach ($options as $op) {
            //Descobrir o $product_option_value com base no option_id e value da Opção
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_value_description` 
                WHERE 
                name = '" . $this->db->escape($op['value']) . "' 
                AND option_id = '" . (int) $option_id . "' 
                ORDER BY option_value_id ASC LIMIT 1
            ");

            if (empty($query->row['option_value_id'])) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET 
                    option_id = '" . (int) $option_id . "', 
                    image = '', 
                    sort_order = '" . (int) $sort_order . "'
                ");

                $option_value_id = $this->db->getLastId();

                $sort_order++;

                foreach ($this->languages as $l) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET 
                        option_value_id = '" . (int) $option_value_id . "', 
                        language_id = '" . (int) $l['language_id'] . "', 
                        option_id = '" . (int) $option_id . "', 
                        name = '" . $this->db->escape($op['value']) . "'
                    ");
                }
            } else {
                $option_value_id = $query->row['option_value_id'];
            }

            $this->log->write('addOptionDatabase -  Adicionando no Banco, option_value_id: ' . $option_value_id);

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET 
                product_option_id = '" . (int) $product_option_id . "', 
                product_id = '" . (int) $product_id . "', 
                option_id = '" . (int) $option_id . "', 
                option_value_id = '" . (int) $option_value_id . "', 
                quantity = '" . (int) $op['quantity'] . "', 
                subtract = '" . (int) $subtract . "', 
                price = '" . (float) $price . "', 
                price_prefix = '" . $this->db->escape($price_prefix) . "', 
                points = '" . (int) $points . "', 
                points_prefix = '" . $this->db->escape($points_prefix) . "', 
                weight = '" . (float) $weight . "', 
                weight_prefix = '" . $this->db->escape($weight_prefix) . "'
            ");

            $this->log->write('addOptionDatabase -  Adicionado no Banco');
        }
    }

    /**
     *
     * Adiciona o Preço Promocional no Produto
     * Sem uso no momento
     *
     * @param $product_id
     * @param $group
     * @param $priority
     * @param $price
     * @param $date_start
     * @param $date_end
     */
    private function addSpecialDatbase($product_id, $group, $priority, $price, $date_start, $date_end)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET
            product_id = '" . (int) $product_id . "',
            customer_group_id = '" . (int) $group . "',
            priority = '" . (int) $priority . "',
            price = '" . (float) $price . "',
            date_start = '" . $this->db->escape($date_start) . "',
            date_end = '" . $this->db->escape($date_end) . "'
        ");
    }

    /**
     * Atualizando a quantidade e preço dos Produtos
     *
     * @param $product_id
     * @param $product
     *
     * @return bool
     */
    private function editProductDatabase($product_id, $product)
    {
        $this->log->write('editProductDatabase -  Alterando o Preço e Quantidade do Produto: ' . $product_id);
        echo "Alterando Quantidade e Preço do Produto $product_id<br>";

        $data = $this->mountPriceQuantity($product);
        if (empty($data['price'])) {
            return false;
        }

        $price = $data['price'];
        $stockTotal = $data['stockTotal'];
        $options = $data['options'];

        if (!empty($this->conf->code_sincronizar_preco) && $this->conf->code_sincronizar_preco == 1) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET 
                quantity = '" . (int) $stockTotal . "', 
                price = '" . (float) $price . "',
                date_modified = NOW()
                WHERE 
                product_id = '" . (int) $product_id . "'
            ");

            $this->log->write('editProductDatabase -  Alterado preço: ' . (float) $price . ', Produto: ' . $product_id);
            $this->log->write('editProductDatabase -  Alterado a quantidade: ' . (int) $stockTotal . ', Produto: ' . $product_id);

            echo "Alterado Quantidade $stockTotal e Preço $price do Produto $product_id<br>";
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET 
                quantity = '" . (int) $stockTotal . "', 
                date_modified = NOW()
                WHERE 
                product_id = '" . (int) $product_id . "'
            ");

            echo "Alterando Quantidade $stockTotal do Produto $product_id<br>";
            $this->log->write('editProductDatabase -  Alterado a quantidade: ' . (int) $stockTotal . ', Produto: ' . $product_id);
        }

        //Atualizando as quantidades das Opções
        $option_id = $this->conf->code_opcao_id;

        foreach ($options as $op) {
            //Descobrir o $product_option_value com base no option_id e value da Opção
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option_value_description` 
                WHERE 
                name = '" . $this->db->escape($op['value']) . "' 
                AND option_id = '" . (int) $option_id . "' 
                ORDER BY option_value_id ASC LIMIT 1
            ");

            if (!empty($query->row['option_value_id'])) {
                $option_value_id = $query->row['option_value_id'];

                $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET 
                    quantity = '" . (int) $op['quantity'] . "'
                    WHERE 
                    option_value_id = '" . (int) $option_value_id . "' and product_id = '" . (int) $product_id . "'
                ");

                echo "Alterado Quantidade: " . $op['quantity'] . " Option Value ID: $option_value_id, Produto: $product_id <br>";
                $this->log->write('editProductDatabase -  Adicionado no Banco a Quantidade: ' . $op['quantity'] . ' Option Value ID: ' . $option_value_id . ', Produto: ' . $product_id);
            }
        }

        //Alterando os dados na tabela da revenda
        $this->db->query("UPDATE " . DB_PREFIX . "code_revendadecalcados_product SET
            revenda_price_atacado = '" . $product['valor_atacado'] . "',
            revenda_price_dropshipping = '" . $product['valor_dropshipping'] . "',
            revenda_quantity = '" . (int) $stockTotal . "',
            revenda_status = '" . (int) $product['reposicao_estoque'] . "',
            data = '" . $this->db->escape(json_encode($product)) . "',
            date_modified = NOW()
            WHERE 
            product_id = '" . (int) $product_id . "'
        ");

        $this->log->write('editProductDatabase -  Rodado com sucesso Produto: ' . $product_id);

        return true;
    }

    /**
     * Verfica se existe a caregoria e cria ela
     *
     * @param $p
     *
     * @return array
     */
    private function addCategory($p)
    {
        if (empty($p['categoria'])) {
            return [];
        }

        //Verificar se existe a Categoria
        //Criar a Categoria
        //Ligar o Produto a Categoria

        //Verificando se existe a categoria
        $query = $this->db->query("SELECT c . category_id FROM `" . DB_PREFIX . "code_revendadecalcados_category` rc
            INNER JOIN " . DB_PREFIX . "category c ON(c . category_id = rc . category_id)
            WHERE  rc . name = '" . $this->db->escape($p['categoria']) . "' LIMIT 1
        ");

        if (!empty($query->row['category_id'])) {
            $this->log->write('addCategory - Categoria: ' . $query->row['category_id'] . ', nome: ' . $p['categoria'] . ' já existe');
            $categorias[] = $query->row['category_id'];
        } else {
            foreach ($this->languages as $l) {
                $lin[$l['language_id']] = [
                    'name'             => trim($p['categoria']),
                    'description'      => trim($p['categoria']),
                    'meta_title'       => trim($p['categoria']),
                    'meta_description' => trim($p['categoria']),
                    'meta_keyword'     => trim($p['categoria']),
                ];
            }

            $parente = '';
            $parent_id = 0;
            $top = 1;

            $data = [
                'category_description' => $lin,
                'path'                 => $parente,
                'parent_id'            => $parent_id,
                'filter'               => '',
                'category_store'       => [0],
                'keyword'              => $this->slug(trim($p['categoria'])),
                'name'                 => trim($p['categoria']),
                'image'                => '',
                'top'                  => $top,
                'column'               => 1,
                'sort_order'           => 1,
                'status'               => !empty($this->conf->code_habilitar_categorias) && $this->conf->code_habilitar_categorias == 1 ? 1 : 0,
                'category_layout'      => [''],
            ];

            $categoria_id = $this->addCategoryDatabase($data);

            $this->log->write('addCategory - Categoria: ' . $categoria_id . ', nome:' . $p['categoria'] . ' criada com sucesso');
            $categorias[] = $categoria_id;
        }

        return $categorias;
    }

    /**
     * @param $data
     *
     * @return int
     */
    private function addCategoryDatabase($data)
    {
        $this->log->write('addCategoryDatabase() - Criando');

        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        $this->log->write('addCategoryDatabase() - Criada, ID: ' . $category_id);

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int) $category_id . "'");
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");

            if (isset($data['keyword'])) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = 0, language_id = '" . (int) $language_id . "', query = 'category_id=" . (int) $category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', `level` = '" . (int) $level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', `level` = '" . (int) $level . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "code_revendadecalcados_category SET
            category_id = '" . (int) $category_id . "',
            name = '" . $this->db->escape($data['name']) . "'
        ");

        return $category_id;
    }

    /**
     * Monta para adicionar um Fabricante
     *
     * @param $nome
     *
     * @return int
     */
    private function fabricante($nome)
    {
        //Verificando se já não foi criado
        $query = $this->db->query("SELECT m . manufacturer_id FROM `" . DB_PREFIX . "code_revendadecalcados_manufacturer` rc
            INNER JOIN " . DB_PREFIX . "manufacturer m ON(m . manufacturer_id = rc . manufacturer_id)
            WHERE  rc . name = '" . $this->db->escape($nome) . "' LIMIT 1
        ");

        if (isset($query->row['manufacturer_id'])) {
            $this->log->write('fabricante() - Já cadastrado');
            return $query->row['manufacturer_id'];
        }

        $data = [
            'name'               => $nome,
            'manufacturer_store' => [0],
            'keyword'            => $this->slug($nome),
            'sort_order'         => 1,
        ];

        //Salvando no Banco de dados
        return $this->addManufacturer($data);
    }

    /**
     * Adiciona o Fabricante
     *
     * @param array $data
     *
     * @return int $manufacturer_id
     */
    private function addManufacturer($data)
    {
        $this->log->write('addManufacturer() - Criando');

        $this->db->query("
            INSERT INTO " . DB_PREFIX . "manufacturer 
            SET name = '" . $this->db->escape($data['name']) . "',
            sort_order = '" . (int) $data['sort_order'] . "'
        ");

        //Id do Fabricante criado
        $manufacturer_id = $this->db->getLastId();

        $this->db->query("
            INSERT INTO " . DB_PREFIX . "code_revendadecalcados_manufacturer SET
            manufacturer_id = '" . $manufacturer_id . "',
            name = '" . $this->db->escape($data['name']) . "'
        ");

        if (isset($data['manufacturer_store'])) {
            foreach ($data['manufacturer_store'] as $store_id) {
                $this->db->query("
                    INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET 
                    manufacturer_id = '" . (int) $manufacturer_id . "',
                    store_id = '" . (int) $store_id . "'
                ");
            }
        }

        if (isset($data['keyword'])) {
            $this->db->query("
                INSERT INTO " . DB_PREFIX . "seo_url SET
                store_id = 0, 
                language_id = 2,
                query = 'manufacturer_id=" . (int) $manufacturer_id . "',
                keyword = '" . $this->db->escape($data['keyword']) . "'
            ");
        }

        $this->log->write('addManufacturer() - Fabricante criado, ID: ' . $manufacturer_id);

        return $manufacturer_id;
    }

    /**
     * Retorna o grupo de Clientes, usado para Promoção
     *
     * @param array $data
     *
     * @return mixed
     */
    private function getCustomerGroups($data = [])
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON(cg . customer_group_id = cgd . customer_group_id) WHERE cgd . language_id = '" . (int) $this->config->get('config_language_id') . "'";

        $sort_data = [
            'cgd.name',
            'cg.sort_order',
        ];

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cgd . name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
    //-------- FIM MÉTODOS NO OPENCART --------

    /** Cria o Slug
     *
     * @param $str
     * @param array $replace
     * @param string $delimiter
     *
     * @return string $slug
     */
    private function slug($str, $replace = [], $delimiter = '-')
    {
        $table = [
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c', 'Ć' => 'C', 'ć' => 'c',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b',
            'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r',
        ];

        $str = strtr($str, $table);

        //Script criado por chluehr https://gist.github.com/chluehr/1632883
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace(" / [^a - zA - Z0 - 9\/_ | +-]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace(" / [\/_ | +-]+/", $delimiter, $clean);

        return $clean;
        //Fim do script por chluehr
    }
}