<?php

require_once DIR_SYSTEM . '/library/code_mp/vendor/autoload.php';

/**
 * Class ControllerExtensionPaymentCodeMP
 * © Copyright 2014-2020 Codemarket - Todos os direitos reservados.
 *
 * @author Codemarket - Felipo Antonoff Araújo e Victor Henrique Ramos
 * @version 1.0
 * @package code_mp
 */
class ControllerExtensionPaymentCodeMP extends Controller
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $log;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->log = new Log('Code-MercadoPago-Cartao.log');
        $this->conf = $this->getConfig();

        if (empty($this->conf)) {
            return false;
            exit();
        }

        $this->load->model('checkout/order');
    }

    /**
     * Retorna as configuracoes do painel
     *
     * @return \stdClass
     */
    private function getConfig()
    {
        $config = new stdClass();

        $this->load->model('module/codemarket_module');
        $config = $this->model_module_codemarket_module->getModulo('562');
        //https://www.mercadopago.com.br/developers/pt/guides/payments/api/handling-responses/ e https://www.mercadopago.com.br/developers/pt/reference/payments/_payments_search/get/

        if (empty($config) || empty($config->code_production_key) || empty($config->code_production_token)
            || empty($config->code_sandbox_key) || empty($config->code_sandbox_token)
            || empty($config->code_title_cartao) || empty($config->code_env_cartao) || !isset($config->code_sort_order_cartao)
            || empty($config->code_status_cartao) || $config->code_status_cartao == 2
        ) {
            $this->log->write('getConfig() - Módulo desativado ou configurado errado. Verificar a Configuração do módulo');
            return false;
        }

        if ($config->code_env_cartao === 'production') {
            $config->access_token = $config->code_production_token;
            $config->key = $config->code_production_key;
        } else {
            $config->access_token = $config->code_sandbox_token;
            $config->key = $config->code_sandbox_key;
        }

        $config->code_text_error = 'Problemas com seu pagamento, tente novamente mais tarde ou selecione outra forma de pagamento.';

        //Cartão
        $config->code_texto_cartao = isset($config->code_mensagem_cartao) ? $config->code_mensagem_cartao : '';
        $config->code_botao_cartao = isset($config->code_botao_cartao) ? $config->code_botao_cartao : 'Confirmar Pagamento e Pedido';

        //TESTE APENAS
        //$config->code_custom_cpf = 1;
        //$config->code_custom_cpf2 = 1;

        return $config;
    }

    /**
     * Carregamento da view
     *
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        $data['order'] = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $nome = $data['order']['firstname'];
        $nome .= isset($data['order']['lastname']) ? ' ' . $data['order']['lastname'] : '';
        $data['titular'] = strtoupper($nome);

        // Adicionando o CPF se existir
        $data['cpf'] = '';
        if (!empty($this->conf->code_custom_cpf) && !empty($data['order']['custom_field'][$this->conf->code_custom_cpf])) {
            //Removendo máscaras e adicionando máscara correta no CPF
            $cpf = preg_replace("/[^0-9]/", "", $data['order']['custom_field'][$this->conf->code_custom_cpf]);

            if (strlen($cpf) == 11) {
                $cpf = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpf);
                $data['cpf'] = trim($cpf);
            }
        } else if (!empty($this->conf->code_custom_cpf2) && !empty($data['order']['custom_field'][$this->conf->code_custom_cpf2])) {
            //Removendo máscaras e adicionando máscara correta no CPF
            $cpf = preg_replace("/[^0-9]/", "", $data['order']['custom_field'][$this->conf->code_custom_cpf2]);

            if (strlen($cpf) == 11) {
                $cpf = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpf);
                $data['cpf'] = trim($cpf);
            }
        }

        $data['key'] = $this->conf->key;
        $data['code_texto_cartao'] = $this->conf->code_texto_cartao;
        $data['code_botao_cartao'] = $this->conf->code_botao_cartao;
        $data['confirm'] = $this->url->link('extension/payment/code_mp/confirm', '', true);
        $data['code_css'] = $this->conf->code_css;

        return $this->load->view('extension/payment/code_mp', $data);
    }

    /**
     * Confirmar do Cartão - Cria o Pagamento e muda o Status na Fatura para o criado
     *
     * @throws \Exception
     */
    public function confirm()
    {
        if ($this->session->data['payment_method']['code'] != 'code_mp') {
            exit();
        }

        $post = json_decode(file_get_contents('php://input'));
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        if (empty($post) || empty($order['order_id']) || empty($post->cc->parcelas) || empty($post->cc->paymentMethodId) || empty($post->parcelamento->issuer->id)
            || empty($post->parcelamento->payer_costs[((int) $post->cc->parcelas - 1)]->total_amount)) {
            $this->log->write('Confirm() - Pagamento negado, sem os dados do Cartão ou sem os dados do Pedido');

            //Debug
            //$this->log->write('Post Dados');
            //$this->log->write(json_encode($post, JSON_PRETTY_PRINT));

            exit();
        }

        MercadoPago\SDK::setAccessToken($this->conf->access_token);

        //https://www.mercadopago.com.br/developers/pt/reference/payments/_payments/post/
        $payment = new MercadoPago\Payment();

        /*
        {
        "cc": {
            "number": "4111...",
            "exp": "11 \/ 19",
            "cvc": "123",
            "name": "Codemarket Teste",
            "email": "teste@codemarket.com.br",
            "cpf": "709.265...",
            "paymentMethodId": "visa",
            "exp_month": "11",
            "exp_year": "2019",
            "parcelas": 4,
            "typeDoc": "CPF"
        },
        "token": "f0b9623eeee1b39365f2f61",
        "parcelamento": {
            "payment_method_id": "visa",
            "payment_type_id": "credit_card",
            "issuer": {
                "id": "25",
                "name": "Visa",
                "secure_thumbnail": "https:\/\/www.mercadopago.com\/org-img\/MP3\/API\/logos\/visa.gif",
                "thumbnail": "http:\/\/img.mlstatic.com\/org-img\/MP3\/API\/logos\/visa.gif"
            },
            "processing_mode": "aggregator",
            "merchant_account_id": null,
            "payer_costs": [
                {
                    "installments": 1,
                    "installment_rate": 0,
                    "discount_rate": 0,
                    "reimbursement_rate": null,
                    "labels": [],
                    "installment_rate_collector": [
                        "MERCADOPAGO"
                    ],
                    "min_allowed_amount": 0,
                    "max_allowed_amount": 60000,
                    "recommended_message": "1 parcela de R$ 308,00 (R$ 308,00)",
                    "installment_amount": 308,
                    "total_amount": 308
                },
                {
                    "installments": 2,
                    "installment_rate": 2.39,
                    "discount_rate": 0,
                    "reimbursement_rate": null,
                    "labels": [],
                    "installment_rate_collector": [
                        "MERCADOPAGO"
                    ],
                    "min_allowed_amount": 10,
                    "max_allowed_amount": 60000,
                    "recommended_message": "2 parcelas de R$ 157,68 (R$ 315,36)",
                    "installment_amount": 157.68,
                    "total_amount": 315.36
                },
                ...
                {
                    "installments": 12,
                    "installment_rate": 17.41,
                    "discount_rate": 0,
                    "reimbursement_rate": null,
                    "labels": [],
                    "installment_rate_collector": [
                        "MERCADOPAGO"
                    ],
                    "min_allowed_amount": 60,
                    "max_allowed_amount": 60000,
                    "recommended_message": "12 parcelas de R$ 30,14 (R$ 361,62)",
                    "installment_amount": 30.14,
                    "total_amount": 361.62
                }
            ],
            "agreements": null
        }
        }
        */

        /*
        * stdClass Object
        (
            [installments] => 4
            [installment_rate] => 7.17
            [discount_rate] => 0
            [reimbursement_rate] =>
            [labels] => Array
                (
                )

            [installment_rate_collector] => Array
                (
                    [0] => MERCADOPAGO
                )

            [min_allowed_amount] => 20
            [max_allowed_amount] => 60000
            [recommended_message] => 4 parcelas de R$ 82,52 (R$ 330,08)
            [installment_amount] => 82.52
            [total_amount] => 330.08
        )
        */
        $parcelamentoData = $post->parcelamento->payer_costs[((int) $post->cc->parcelas - 1)];

        $payment->transaction_amount = $this->currency->format($order['total'], $order['currency_code'], false, false);
        $payment->token = $post->token;
        $payment->statement_descriptor = $this->conf->code_statement_descriptor;
        $payment->description = "Pedido #" . $order['order_id'] . " - " . $this->config->get('config_name');
        $payment->installments = (int) $post->cc->parcelas;
        $payment->payment_method_id = $post->cc->paymentMethodId;
        $payment->issuer_id = (int) $post->parcelamento->issuer->id;

        //Usando o CPF digitado no Cartão
        $cpf = isset($post->cc->cpf) ? preg_replace("/[^0-9]/", "", $post->cc->cpf) : '';
        $identification = [
            "type"   => "CPF",
            "number" => $cpf,
        ];

        //Dados do cliente
        $payment->payer = [
            "email"          => $order['email'],
            "first_name"     => $order['firstname'],
            "last_name"      => $order['lastname'],
            "identification" => $identification,
            "address"        => [
                "zip_code"      => preg_replace("/[^0-9]/", "", $order['payment_postcode']),
                "street_name"   => $order['payment_address_1'],
                "street_number" => isset($order['payment_custom_field'][$this->conf->code_custom_number]) ? preg_replace("/[^0-9]/", "", $order['payment_custom_field'][$this->conf->code_custom_number]) : '',
                "neighborhood"  => $order['payment_address_2'],
                "city"          => $order['payment_city'],
            ],
        ];

        $payment->external_reference = $order['order_id'];
        $payment->notification_url = $this->url->link('extension/payment/code_mp/webhook', '', true);

        // Save and posting the payment
        $payment->save();

        if (!empty($payment->transaction_details)) {
            $this->log->write('Confirm() - Pedido criado');
            $this->log->write('Confirm() - Pedido criado, dados: ' . print_r($payment->transaction_details, true));

            $json['url'] = $payment->transaction_details->external_resource_url;
            $json['redirect'] = $this->url->link('checkout/success');

            $statusAlert = false;
            if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
                $statusAlert = true;
            }

            $bandeira = $post->parcelamento->issuer->name;

            $comment = "Pagamento por Cartão " . $bandeira;
            $comment .= "<br>";
            $comment .= "Primeiros números do Cartão: " . substr($post->cc->number, 0, 9);
            $comment .= "<br>";
            $comment .= $parcelamentoData->recommended_message;
            $comment .= "<br><br>";
            $comment .= "Dados do Titular do Cartão:";
            $comment .= "<br>";
            $comment .= "Nome: " . $post->cc->name;
            $comment .= "<br>";
            $comment .= "CPF: " . $post->cc->cpf;

            $this->model_checkout_order->addOrderHistory(
                (int) $order['order_id'],
                (int) $this->conf->code_created,
                $comment,
                $statusAlert
            );
        } else {
            $this->log->write('Generate() - Pagamento falhou, veja abaixo os erros');
            $this->log->write(json_encode(['order_id: ' . $order['order_id'], $payment->error], JSON_PRETTY_PRINT));
            $this->log->write('Confirm() - Pagamento falhou, dados payment' . print_r($payment, true));

            if ($payment->error->message == 'Invalid user identification number') {
                $payment->error->message = 'Documento inválido, por favor verificar o CPF ou CNPJ usado';
            } else {
                $payment->error->message = $this->conf->code_text_error;
            }

            $json['error'] = $payment->error->message;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * Notificação de Pagamento
     *
     * @return bool|void
     */
    public function webhook()
    {
        sleep(5);
        $log = new Log('Code-MercadoPago-Webhook.log');

        $log->write('Inicio');
        $post = json_decode(file_get_contents('php://input'));

        if (!isset($this->conf->code_status_notify)
            || !isset($this->conf->code_created) || !isset($this->conf->code_pending) || !isset($this->conf->code_approved)
            || !isset($this->conf->code_authorized) || !isset($this->conf->code_in_process) || !isset($this->conf->code_in_mediation)
            || !isset($this->conf->code_rejected) || !isset($this->conf->code_cancelled) || !isset($this->conf->code_refunded)
            || !isset($this->conf->code_charged_back)
        ) {
            $log->write('Status não configurados corretamente, verifique a Configuração do módulo');
            return false;
            exit();
        }

        //Json Recebido
        $log->write('Post Dados');
        $log->write(json_encode($post, JSON_PRETTY_PRINT));

        if (empty($post->type) || $post->type != 'payment' || empty($post->action) || $post->action == 'payment.created') {
            $log->write('Post sem os dados necessários ou para Pedido criado já notificado');
            return false;
            exit();
        }

        MercadoPago\SDK::setAccessToken($this->conf->access_token);
        $payment = MercadoPago\Payment::find_by_id($post->data->id);

        //Json do pagamento
        /*
        $log->write('Json Pagamento');
        $log->write(print_r($payment, true));
        */

        if (empty($payment->external_reference) || empty($payment->status)) {
            $log->write('Pagamento sem os dados necessários do ID e Status ' . print_r($payment, true));
            return false;
            exit();
        }

        $orderId = (int) $payment->external_reference;

        $statusMP = trim(strtolower($payment->status));
        $confStatus = 'code_' . $statusMP;

        if (!empty($this->conf->{$confStatus})) {
            $statusId = $this->conf->{$confStatus};
            $log->write("Status ID: " . $statusId);
        } else {
            $log->write('Status não encontrado na configuração');
            return false;
            exit();
        }

        //Verificando se já tem o Status no Pedido
        $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
            WHERE 
            order_id = '" . (int) $orderId . "' 
            AND 
            order_status_id = '" . (int) $statusId . "'
            LIMIT 1
         ");

        $log->write("Retorno da verificação: " . print_r($query->row, true));

        if (!empty($query->row['order_id'])) {
            $log->write("Pedido: " . $orderId . ", já notificado");

            $json = [
                'error' => 'Pedido já notificado com esse status',
            ];

            $this->response->addHeader('Content-Type: application/json');
            return $this->response->setOutput(json_encode($json));
            exit();
        }

        $statusAlert = false;
        if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
            $statusAlert = true;
        }

        $this->model_checkout_order->addOrderHistory(
            $orderId,
            $statusId,
            '',
            $statusAlert
        );

        $log->write("Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP);

        $this->response->setOutput('OK');
    }

    /**
     * Atualiza os Status puxando diretamente da API do Mercado Pago, recomendado só se tiver problemas com o Webhook automático
     * URL: sualoja.com.br/index.php?route=extension/payment/code_mp/webhookCron&token=seutoken
     *
     * @throws \Exception
     */
    public function webhookCron()
    {
        $log = new Log('Code-MercadoPago-Webhook-Cronjob.log');

        if (empty($this->request->get['token']) || empty($this->conf->code_cron_job) || $this->request->get['token'] != $this->conf->code_cron_job) {
            $log->write('Token inválido');
            exit('Token inválido!');
        }

        $log->write('Inicio');
        echo "Iniciado Verificação <br><br>";

        if (!isset($this->conf->code_status_notify)
            || !isset($this->conf->code_created) || !isset($this->conf->code_pending) || !isset($this->conf->code_approved)
            || !isset($this->conf->code_authorized) || !isset($this->conf->code_in_process) || !isset($this->conf->code_in_mediation)
            || !isset($this->conf->code_rejected) || !isset($this->conf->code_cancelled) || !isset($this->conf->code_refunded)
            || !isset($this->conf->code_charged_back)
        ) {
            $log->write('Status não configurados corretamente, verifique a Configuração do módulo');
            exit("Status não configurados corretamente, verifique a Configuração do módulo");
        }

        $dayIntervalLimit = 5;

        //Não verifica Status com ID 0 ou com Status ID do $this->config->get('config_fraud_status_id') normalmente Cancelado
        $query = $this->db->query("
            SELECT order_id, order_status_id, payment_code, date_modified, NOW()
            FROM `" . DB_PREFIX . "order`
            WHERE
            (payment_code = 'code_mp' OR payment_code = 'code_mp_boleto') AND
            order_status_id != 0 AND 
            order_status_id !=  '" . $this->config->get('config_fraud_status_id') . "' AND
            (NOW() - INTERVAL '" . (int) $dayIntervalLimit . "' DAY) <= date_modified
        ");

        //print_r($query);

        if (empty($query->row['order_id'])) {
            exit('Sem Pedido retornado');
        }

        MercadoPago\SDK::setAccessToken($this->conf->access_token);

        foreach ($query->rows as $order) {
            $orderId = (int) $order['order_id'];
            $log->write('Verificando Pedido: ' . $orderId);

            if ($order['payment_code'] == "code_mp") {
                $payment_type_id = 'credit_card';
            } else {
                $payment_type_id = 'ticket';
            }

            $payments = MercadoPago\Payment::search([
                "external_reference" => $orderId,
                "payment_type_id"    => $payment_type_id,
                "limit"              => 1,
                "offset"             => 0,
                "sort"               => "date_last_updated",
                "criteria"           => "desc",
            ]);

            $payment = end($payments);

            if (empty($payment->external_reference) && !empty($payments[0]->external_reference)) {
                $payment = $payments[0];
            }

            if (empty($payment->external_reference) || empty($payment->status) || $payment->external_reference != $orderId) {
                $log->write('Pagamento sem os dados necessários do ID, Status ou ID diferente do Pedido ' . print_r($payment, true));
                continue;
            }

            $statusMP = trim(strtolower($payment->status));
            $confStatus = 'code_' . $statusMP;

            if (!empty($this->conf->{$confStatus})) {
                $statusId = $this->conf->{$confStatus};
                $log->write("Status ID: " . $statusId);
            } else {
                $log->write('Status não encontrado na configuração');
                continue;
            }

            //Verificando se já tem o Status no Pedido
            $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
            WHERE 
            order_id = '" . (int) $orderId . "' 
            AND 
            order_status_id = '" . (int) $statusId . "'
            LIMIT 1
         ");

            $log->write("Retorno da verificação: " . print_r($query->row, true));

            if (!empty($query->row['order_id'])) {
                $log->write("Pedido: " . $orderId . ", já notificado");
                echo "Pedido: " . $orderId . ", já notificado";
                echo "<br>";
                continue;
            }

            $statusAlert = false;
            if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
                $statusAlert = true;
            }

            $this->model_checkout_order->addOrderHistory(
                $orderId,
                $statusId,
                '',
                $statusAlert
            );

            $log->write("Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP);
            echo "Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP;
            echo "<br>";
        }

        echo "<br> Verificação de Status rodada com sucesso!";
    }
}
