<?php

require_once DIR_SYSTEM . '/library/code_mp/vendor/autoload.php';

/**
 * Class ControllerExtensionPaymentCodeMP
 * © Copyright 2014-2020 Codemarket - Todos os direitos reservados.
 *
 * @author Codemarket - Felipo Antonoff Araújo e Victor Henrique Ramos
 * @version 1.0
 * @package code_mp
 */
class ControllerExtensionPaymentCodeMPBoleto extends Controller
{
    /**
     * @var \stdClass
     */
    private $conf;
    private $log;

    /**
     * Definicoes iniciais de boot
     *
     * @param $registry
     */
    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->log = new Log('Code-MercadoPago-Boleto.log');
        $this->conf = $this->getConfig();

        if (empty($this->conf)) {
            return false;
        }

        $this->load->model('checkout/order');
    }

    /**
     * Retorna as configuracoes do painel
     *
     * @return \stdClass
     */
    private function getConfig()
    {
        $config = new stdClass();

        $this->load->model('module/codemarket_module');
        $config = $this->model_module_codemarket_module->getModulo('562');
        //https://www.mercadopago.com.br/developers/pt/guides/payments/api/handling-responses/ e https://www.mercadopago.com.br/developers/pt/reference/payments/_payments_search/get/

        if (empty($config) || empty($config->code_production_key) || empty($config->code_production_token)
            || empty($config->code_sandbox_key) || empty($config->code_sandbox_token) || empty($config->code_custom_cpf)
            || empty($config->code_title_boleto) || !isset($config->code_vencimento) || empty($config->code_env_boleto)
            || !isset($config->code_sort_order_boleto) || empty($config->code_status_boleto) || $config->code_status_boleto == 2
        ) {
            $this->log->write('getConfig() - Módulo desativado ou configurado errado. Verificar a Configuração do módulo');
            return false;
        }

        if ($config->code_env_boleto === 'production') {
            $config->access_token = $config->code_production_token;
            $config->key = $config->code_production_key;
        } else {
            $config->access_token = $config->code_sandbox_token;
            $config->key = $config->code_sandbox_key;
        }

        $config->code_text_error = 'Problemas com seu pagamento, tente novamente mais tarde ou selecione outra forma de pagamento.';

        //Boleto
        $config->code_texto_boleto = isset($config->code_mensagem_boleto) ? $config->code_mensagem_boleto : '';
        $config->code_text_success_boleto = "Boleto gerado, clique em <b>Abrir Boleto</b> para visualizar agora ou <b>Fechar</b> para continuar!";
        $config->code_botao_boleto = isset($config->code_botao_boleto) ? $config->code_botao_boleto : 'Confirmar Pagamento e Pedido';

        //TESTE APENAS
        //$config->code_custom_cpf = 1;
        //$config->code_custom_cpf2 = 1;

        return $config;
    }

    /**
     * Carregamento da view
     *
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        $data['code_texto_boleto'] = $this->conf->code_texto_boleto;
        $data['code_text_success_boleto'] = $this->conf->code_text_success_boleto;
        $data['text_code_loading'] = 'Carregando...';
        $data['code_botao_boleto'] = $this->conf->code_botao_boleto;
        $data['confirm'] = $this->url->link('extension/payment/code_mp_boleto/confirm', '', true);
        $data['code_css'] = $this->conf->code_css;

        return $this->load->view('extension/payment/code_mp_boleto', $data);
    }

    /**
     *
     * Confirmar do Boleto - Cria o Pagamento e muda o Status na Fatura para o criado
     *
     * @throws \Exception
     */
    public function confirm()
    {
        if ($this->session->data['payment_method']['code'] != 'code_mp_boleto') {
            exit();
        }

        //$post = json_decode(file_get_contents('php://input'));
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        MercadoPago\SDK::setAccessToken($this->conf->access_token);

        $payment = new MercadoPago\Payment();

        $payment->transaction_amount = $this->currency->format($order['total'], $order['currency_code'], false, false);
        $payment->description = "Pedido #" . $order['order_id'] . " - " . $this->config->get('config_name');

        //$date = \Carbon\Carbon::now('America/Sao_Paulo')->addDays((int) $this->conf->code_vencimento)->format('Y-m-d\TH:i:s.uP');
        $date = new DateTime(date('Y-m-d\TH:i:s.uP'), new DateTimeZone('America/Sao_Paulo'));
        $date->add(new DateInterval('P' . $this->conf->code_vencimento . 'D'));
        $date = $date->format('Y-m-d\TH:i:s.uP');
        //2019-10-18T15:43:36.000000-03:00
        $exc = '.' . substr($date, 23, 3);
        $date = str_replace($exc, '.', $date);
        //2019-10-18T15:46:08.000-03:00
        $payment->date_of_expiration = $date;
        $payment->payment_method_id = "bolbradesco";

        $cpf = isset($order['custom_field'][$this->conf->code_custom_cpf]) ? preg_replace("/[^0-9]/", "", $order['custom_field'][$this->conf->code_custom_cpf]) : '';

        if (strlen($cpf) == 11) {
            $identification = [
                "type"   => "CPF",
                "number" => $cpf,
            ];
        } else if (strlen($cpf) == 14) {
            $identification = [
                "type"   => "CNPJ",
                "number" => $cpf,
            ];
        } else {
            $cpf = isset($order['custom_field'][$this->conf->code_custom_cpf2]) ? preg_replace("/[^0-9]/", "", $order['custom_field'][$this->conf->code_custom_cpf2]) : '';

            if (strlen($cpf) == 11) {
                $identification = [
                    "type"   => "CPF",
                    "number" => $cpf,
                ];
            } else if (strlen($cpf) == 14) {
                $identification = [
                    "type"   => "CNPJ",
                    "number" => $cpf,
                ];
            } else {
                $this->log->write('CPF/CNPJ inválido ' . print_r($cpf, true));

                $json['error'] = 'Documento inválido, por favor verificar o CPF ou CNPJ usado';
                $this->response->addHeader('Content-Type: application/json');
                return $this->response->setOutput(json_encode($json));
            }
        }

        //Dados do cliente
        $payment->payer = [
            "email"          => $order['email'],
            "first_name"     => $order['firstname'],
            "last_name"      => $order['lastname'],
            "identification" => $identification,
            "address"        => [
                "zip_code"      => preg_replace("/[^0-9]/", "", $order['payment_postcode']),
                "street_name"   => $order['payment_address_1'],
                "street_number" => isset($order['payment_custom_field'][$this->conf->code_custom_number]) ? preg_replace("/[^0-9]/", "", $order['payment_custom_field'][$this->conf->code_custom_number]) : '',
                "neighborhood"  => $order['payment_address_2'],
                "city"          => $order['payment_city'],
            ],
        ];

        $payment->external_reference = $order['order_id'];
        $payment->notification_url = $this->url->link('extension/payment/code_mp_boleto/webhook', '', true);

        // Save and posting the payment
        $payment->save();

        if (!empty($payment->transaction_details) && !empty($payment->transaction_details->external_resource_url)) {
            $this->log->write('Confirm() - Pedido criado');
            $this->log->write('Confirm() - Pedido criado, dados: ' . print_r($payment->transaction_details, true));

            $json['url'] = $payment->transaction_details->external_resource_url;
            $json['redirect'] = $this->url->link('checkout/success');

            $statusAlert = false;
            if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
                $statusAlert = true;
            }

            $comment = "Pagamento por Boleto, clique em Abrir Boleto para visualizar o Boleto<br>";
            $comment .= '<b><a href="' . $json['url'] . '" target="_blank">Abrir Boleto</a></b>';
            $comment .= "<br> Caso já tenha pago, aguarde a confirmação do pagamento";

            $boletoSuccess = "Pagamento por Boleto, clique em Abrir Boleto para visualizar o Boleto<br>";
            $boletoSuccess .= '<a class="btn btn-primary code_mp_boleto_abrir" href="' . $json['url'] . '" target="_blank">Abrir Boleto</a>';
            $boletoSuccess .= "<br><br>";

            $this->session->data['code_mp_boleto'] = $boletoSuccess;

            $this->model_checkout_order->addOrderHistory(
                (int) $order['order_id'],
                (int) $this->conf->code_created,
                $comment,
                $statusAlert
            );
        } else {
            $this->log->write('Confirm() - Pagamento falhou, veja abaixo os erros');
            $this->log->write(json_encode(['order_id: ' . $order['order_id'], $payment->error], JSON_PRETTY_PRINT));
            $this->log->write('Confirm() - Pagamento falhou, dados payment' . print_r($payment, true));

            if ($payment->error->message == 'Invalid user identification number') {
                $payment->error->message = 'Documento inválido, por favor verificar o CPF ou CNPJ usado';
            } else {
                $payment->error->message = $this->conf->code_text_error;
            }

            $json['error'] = $payment->error->message;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    /**
     * Webhook igual do Cartão
     */

    /**
     * Notificação de Pagamento
     *
     * @return bool|void
     */
    public function webhook()
    {
        sleep(5);
        $log = new Log('Code-MercadoPago-Webhook.log');

        $log->write('Inicio');
        $post = json_decode(file_get_contents('php://input'));

        if (!isset($this->conf->code_status_notify)
            || !isset($this->conf->code_created) || !isset($this->conf->code_pending) || !isset($this->conf->code_approved)
            || !isset($this->conf->code_authorized) || !isset($this->conf->code_in_process) || !isset($this->conf->code_in_mediation)
            || !isset($this->conf->code_rejected) || !isset($this->conf->code_cancelled) || !isset($this->conf->code_refunded)
            || !isset($this->conf->code_charged_back)
        ) {
            $log->write('Status não configurados corretamente, verifique a Configuração do módulo');
            return false;
            exit();
        }

        //Json Recebido
        $log->write('Post Dados');
        $log->write(json_encode($post, JSON_PRETTY_PRINT));

        if (empty($post->type) || $post->type != 'payment' || empty($post->action) || $post->action == 'payment.created') {
            $log->write('Post sem os dados necessários ou para Pedido criado já notificado');
            return false;
            exit();
        }

        MercadoPago\SDK::setAccessToken($this->conf->access_token);
        $payment = MercadoPago\Payment::find_by_id($post->data->id);

        //Json do pagamento
        /*
        $log->write('Json Pagamento');
        $log->write(print_r($payment, true));
        */

        if (empty($payment->external_reference) || empty($payment->status)) {
            $log->write('Pagamento sem os dados necessários do ID e Status ' . print_r($payment, true));
            return false;
            exit();
        }

        $orderId = (int) $payment->external_reference;

        $statusMP = trim(strtolower($payment->status));
        $confStatus = 'code_' . $statusMP;

        if (!empty($this->conf->{$confStatus})) {
            $statusId = $this->conf->{$confStatus};
            $log->write("Status ID: " . $statusId);
        } else {
            $log->write('Status não encontrado na configuração');
            return false;
            exit();
        }

        //Verificando se já tem o Status no Pedido
        $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
            WHERE 
            order_id = '" . (int) $orderId . "' 
            AND 
            order_status_id = '" . (int) $statusId . "'
            LIMIT 1
         ");

        $log->write("Retorno da verificação: " . print_r($query->row, true));

        if (!empty($query->row['order_id'])) {
            $log->write("Pedido: " . $orderId . ", já notificado");

            $json = [
                'error' => 'Pedido já notificado com esse status',
            ];

            $this->response->addHeader('Content-Type: application/json');
            return $this->response->setOutput(json_encode($json));
            exit();
        }

        $statusAlert = false;
        if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
            $statusAlert = true;
        }

        $this->model_checkout_order->addOrderHistory(
            $orderId,
            $statusId,
            '',
            $statusAlert
        );

        $log->write("Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP);

        $this->response->setOutput('OK');
    }

    /**
     * Atualiza os Status puxando diretamente da API do Mercado Pago, recomendado só se tiver problemas com o Webhook automático
     * URL: sualoja.com.br/index.php?route=extension/payment/code_mp_boleto/webhookCron&token=seutoken
     *
     * @throws \Exception
     */
    public function webhookCron()
    {
        $log = new Log('Code-MercadoPago-Webhook-Cronjob.log');

        if (empty($this->request->get['token']) || empty($this->conf->code_cron_job) || $this->request->get['token'] != $this->conf->code_cron_job) {
            $log->write('Token inválido');
            exit('Token inválido!');
        }

        $log->write('Inicio');
        echo "Iniciado Verificação <br><br>";

        if (!isset($this->conf->code_status_notify)
            || !isset($this->conf->code_created) || !isset($this->conf->code_pending) || !isset($this->conf->code_approved)
            || !isset($this->conf->code_authorized) || !isset($this->conf->code_in_process) || !isset($this->conf->code_in_mediation)
            || !isset($this->conf->code_rejected) || !isset($this->conf->code_cancelled) || !isset($this->conf->code_refunded)
            || !isset($this->conf->code_charged_back)
        ) {
            $log->write('Status não configurados corretamente, verifique a Configuração do módulo');
            exit("Status não configurados corretamente, verifique a Configuração do módulo");
        }

        $dayIntervalLimit = 5;

        //Não verifica Status com ID 0 ou com Status ID do $this->config->get('config_fraud_status_id') normalmente Cancelado
        $query = $this->db->query("
            SELECT order_id, order_status_id, payment_code, date_modified, NOW()
            FROM `" . DB_PREFIX . "order`
            WHERE
            (payment_code = 'code_mp' OR payment_code = 'code_mp_boleto') AND
            order_status_id != 0 AND 
            order_status_id !=  '" . $this->config->get('config_fraud_status_id') . "' AND
            (NOW() - INTERVAL '" . (int) $dayIntervalLimit . "' DAY) <= date_modified
        ");

        //print_r($query);

        if (empty($query->row['order_id'])) {
            exit('Sem Pedido retornado');
        }

        MercadoPago\SDK::setAccessToken($this->conf->access_token);

        foreach ($query->rows as $order) {
            $orderId = (int) $order['order_id'];
            $log->write('Verificando Pedido: ' . $orderId);

            if ($order['payment_code'] == "code_mp") {
                $payment_type_id = 'credit_card';
            } else {
                $payment_type_id = 'ticket';
            }

            $payments = MercadoPago\Payment::search([
                "external_reference" => $orderId,
                "payment_type_id"    => $payment_type_id,
                "limit"              => 1,
                "offset"             => 0,
                "sort"               => "date_last_updated",
                "criteria"           => "desc",
            ]);

            $payment = end($payments);

            if (empty($payment->external_reference) && !empty($payments[0]->external_reference)) {
                $payment = $payments[0];
            }

            if (empty($payment->external_reference) || empty($payment->status) || $payment->external_reference != $orderId) {
                $log->write('Pagamento sem os dados necessários do ID, Status ou ID diferente do Pedido ' . print_r($payment, true));
                continue;
            }

            $statusMP = trim(strtolower($payment->status));
            $confStatus = 'code_' . $statusMP;

            if (!empty($this->conf->{$confStatus})) {
                $statusId = $this->conf->{$confStatus};
                $log->write("Status ID: " . $statusId);
            } else {
                $log->write('Status não encontrado na configuração');
                continue;
            }

            //Verificando se já tem o Status no Pedido
            $query = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "order_history 
            WHERE 
            order_id = '" . (int) $orderId . "' 
            AND 
            order_status_id = '" . (int) $statusId . "'
            LIMIT 1
         ");

            $log->write("Retorno da verificação: " . print_r($query->row, true));

            if (!empty($query->row['order_id'])) {
                $log->write("Pedido: " . $orderId . ", já notificado");
                echo "Pedido: " . $orderId . ", já notificado";
                echo "<br>";
                continue;
            }

            $statusAlert = false;
            if (!empty($this->conf->code_status_notify) and $this->conf->code_status_notify == 1) {
                $statusAlert = true;
            }

            $this->model_checkout_order->addOrderHistory(
                $orderId,
                $statusId,
                '',
                $statusAlert
            );

            $log->write("Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP);
            echo "Pedido notificado com sucesso: " . $orderId . " " . $statusId . " " . $statusMP;
            echo "<br>";
        }

        echo "<br> Verificação de Status rodada com sucesso!";
    }
}
