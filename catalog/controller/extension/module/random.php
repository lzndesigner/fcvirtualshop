<?php
class ControllerExtensionModuleRandom extends Controller {
	public function index($setting) {
		static $module = 50;
		
		$this->load->language('extension/module/random');

		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.min.js');

		$this->load->model('catalog/product');
		$this->load->model('catalog/random');

		$this->load->model('tool/image');

		$data['heading_title'] = $setting['name'];

		$data['type'] = $setting['type'];

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
			);


				// $product_info = $this->model_catalog_product->getProduct($product_id);
		$results = $this->model_catalog_random->getRandomProducts($filter_data);
		if ($results) {
			foreach ($results as $result) {			

				if ($result['price']) {
					if (!is_null($result['special']) && (float)$result['special'] >= 0) {
						$priceProduct = $result['special'];
					} else {
						$priceProduct = $result['price'];
					}
		
					$priceProductNumber = str_replace(".", "", $priceProduct);
	
					$minimum_parcela = '960';
					
					$parc[1] = '1.0';
					$parc[2] = '1.0'; //03297
					$parc[3] = '1.0'; //04444
					$parc[4] = '1.0'; //05618
					$parc[5] = '1.0'; //0299
					$parc[6] = '1.0'; //04508
					$parc[7] = '1.0'; // 06040
					$parc[8] = '1.0'; // 0759
					$parc[9] = '1.0'; // 0915
					$parc[10] = '1.0'; // 1072
					$parc[11] = '1.1231'; // 0342
					$parc[12] = '1.1392'; // 0491
	
					$var = '';
					for ($i = 1; $i <= 10; $i++) {
						$conf = ($priceProductNumber * $parc[$i]) / $i;
						$conf = number_format($conf * '0.01', 4);
						$conf = str_replace(",", "", $conf);
						$conf = number_format($conf, 2, ',', '.');
						$confV = str_replace(",", "", $conf);
						$confV = str_replace(".", "", $confV);
	
						// if ($i == '12') {
						// 	$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (com juros)';
						// 	$parcelamento = $te;
						// }
						if ($confV > $minimum_parcela) {
							$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (sem juros)';
							$parcelamento = $te;
						} else {
							if ($i == '1') {
								$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (sem juros)';
							} else if ($i == '2') {
								$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (sem juros)';
							} else if ($i == '3') {
								$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (sem juros)';
							} else if ($i == '4') {
								$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (sem juros)';
							} else {
								$te = 'ou <b>' . $i . "x de R$ " . $conf . '</b> (com juros)';
							}
							$parcelamento = $te;
						}
					}
				}// if se existe preco
				// Funcao de Parcelamento na Página do produto

				// Funcao de Desconto à vista
				$priceProductNumber = str_replace(".", "", $priceProduct);				
				
				$pctm = 10;
				$valor_descontado = $priceProductNumber - ($priceProductNumber / 100 * $pctm);
				$valor_descontado = number_format($valor_descontado * '0.01', 2);
				$valor_descontado = str_replace(",", "", $valor_descontado);
				$valor_descontado = number_format($valor_descontado, 2, ',', '.');
				
				$price_avista = 'R$ '.$valor_descontado;
				// Funcao de Desconto à vista
				
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'parcelamento'      => $parcelamento,
					'price_avista'      => $price_avista,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
					);
			}
		}		

		$data['module'] = $module++;
		

		if ($data['products']) {
			return $this->load->view('extension/module/random', $data);
		}
	}
}