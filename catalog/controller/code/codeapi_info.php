<?php

class ControllerCodeCodeApiInfo extends Controller
{
    public function index()
    {
        $opencartVersion = VERSION;
        $phpVersion = PHP_VERSION;
        $curlVersion = curl_version();

        header('Content-Type: application/json');

        if (ob_get_level() > 0) {
            ob_flush();
        }

        echo json_encode(
            [
                'opencartVersion' => $opencartVersion,
                'phpVersion' => $phpVersion,
                'curlVersion' => $curlVersion['version'],
            ]
        );

        exit();
    }
}
