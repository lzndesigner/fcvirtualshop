<?php
// Text
$_['text_title']       = '<b>Depósito Bancário ou Transferência (-10%)</b>';
$_['text_instruction'] = 'Instruções';
$_['text_description'] = 'Deposite o valor total do pedido na conta:';
$_['text_payment']     = 'Seu pedido será liberado após identificarmos o pagamento.';